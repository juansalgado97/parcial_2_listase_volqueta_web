/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial2volquetasweb;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import parcial2volquetas.controlador.ListaSE;
import parcial2volquetas.modelo.Nodo;
import parcial2volquetas.modelo.Volqueta;

/**
 *
 * @author SALGADO
 */
@Named(value = "beanVolqueta")
@SessionScoped
public class BeanVolqueta implements Serializable {

    private ListaSE listaSE = new ListaSE();
    private Nodo volquetaAMostrar = new Nodo(new Volqueta());
    private boolean deshabilitarNuevo = true;
    private boolean habilitarBoton = true;

    /**
     * Creates a new instance of BeanVolqueta
     */
    public BeanVolqueta() {
    }

    public boolean isHabilitarBoton() {
        return habilitarBoton;
    }

    public void setHabilitarBoton(boolean habilitarBoton) {
        this.habilitarBoton = habilitarBoton;
    }
    
    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public ListaSE getListaSE() {
        return listaSE;
    }

    public void setListaSE(ListaSE listaSE) {
        this.listaSE = listaSE;
    }

    public Nodo getVolquetaAMostrar() {
        return volquetaAMostrar;
    }

    public void setVolquetaAMostrar(Nodo volquetaAMostrar) {
        this.volquetaAMostrar = volquetaAMostrar;
    }

    //-----METODOS--------------
    public void listarVolquetasMenoresADos(){
        listaSE.listarMenoresDeDosToneladas();
        irAlPrimero();
    }
    
    public void obtenerVolquetaConMayorCapacidad(){
        volquetaAMostrar = listaSE.obtenerVolquetaConMasCapacidad();
    }
    
    public void habilitarCrearVolqueta() {
        deshabilitarNuevo = false;
        volquetaAMostrar = new Nodo(new Volqueta());
    }

    public void guardarVolqueta() {
        listaSE.adicionarNodo(volquetaAMostrar.getDato());
        volquetaAMostrar = new Nodo(new Volqueta());
        deshabilitarNuevo = true;
        irAlPrimero();

    }

    public void irAlPrimero() {
        volquetaAMostrar = listaSE.getCabeza();
    }

    public void irAlSiguiente() {
        if (volquetaAMostrar.getSiguiente() != null) {
            volquetaAMostrar = volquetaAMostrar.getSiguiente();
        }
    }

    public void irAlultimo() {
        volquetaAMostrar = listaSE.irAlUltimo();
    }

    public void cancelarGuardado() {
        deshabilitarNuevo = true;
        irAlPrimero();
    }
    
    public void validarRecorridoIrAlPrimero(){
        if (listaSE.contarNodos() == 0 || volquetaAMostrar == listaSE.getCabeza()) {
            habilitarBoton = false;
        }
        irAlPrimero();
    }
    
    public void validarRecorridoIrAlUltimo(){
        if (listaSE.contarNodos() == 0 || volquetaAMostrar == listaSE.encontrarElUltimo()) {
            habilitarBoton = false;
        }
        irAlultimo();
    }
    
    public void validarRecorridoIrAlSiguiente(){
        if (listaSE.contarNodos() == 0 || volquetaAMostrar == listaSE.encontrarElUltimo()) {
            habilitarBoton = false;
        }
        irAlSiguiente();
    }
    

}
